% Aprendiendo a programar a los ponchazos
% Eloy Espinaco ; Oldani Pablo

# Introducción

## Momento Casciari

## El final de la historia

. . .

![Editor](images/escritorio.png)\ 

## GNU/Linux

- Windows, IDEs y licencias

## Fácil vs Simple

- Interfáz gráfica vs texto

## ¿Cómo es usar GNU/Linux?

> - Ecosistema de aplicaciones
> - Scripts
> - Personalización
> - Distribuciones

------------------

¿Usar Software libre te hace un mejor desarrollador o los buenos desarrolladores usan software libre?

------------------

Sí

## Recursos

- En el principio fue la línea de comandos.

# El día que borramos todo

## Control de versiones

![](images/gitnogithub.png)

## Servicios
![github](images/github-logo.png)
![gitlab](images/gitlab-logo.png)
![bitbucket](images/bitbucket-logo.png)
![heroku](images/heroku-logo.jpg)

<div class="notes">
This is my note.
</div>

-------------------

![git](images/git.png)

## Ejemplo

    git clone git@gitlab.com:olvap/example.git
    cd example
    touch README.md
    git add README.md
    git commit -m "add README"
    git push -u origin master

------

![git commit](images/git_commit.png)

------

## Ambientes de trabajo

> - Dev
> - Stage
> - Production

## Los comandos de Git

> - clone
> - checkout
> - pull
> - merge
> - add
> - commit
> - push

## Recursos
- https://learngitbranching.js.org/

# Integrando el ecosistema

## Las herramientas

> - vim, nano e emacs
> - ssh, curl y wget
> - bash y variables de entorno
> - man, apropos, info
> - find, locate y grep

# Haciendo Aplicaciones grandes

## Testing

## ¿Qué es un test?

---------

~~~ ruby
require_relative 'simple_number'

test "sabe sumar números" do
  assert_equal 4, SimpleNumber.new(2).add(2)
end

test "sabe multiplicar números" do
  assert_equal 6, SimpleNumber.new(2).multiply(3)
end
~~~

---

~~~ ruby
class SimpleNumber
  def initialize(num)
    @x = num
  end

  def add(y)
    @x + y
  end

  def multiply(y)
    @x * y
  end
end
~~~

---

```ruby
RSpec.describe Order do
  it "sums the prices of its line items" do
    order = Order.new

    order.add_entry(LineItem.new(:item => Item.new(
      :price => Money.new(1.11, :USD)
    )))
    order.add_entry(LineItem.new(:item => Item.new(
      :price => Money.new(2.22, :USD),
      :quantity => 2
    )))

    expect(order.total).to eq(Money.new(5.55, :USD))
  end
end
```

## ¿Para que sirve?

> - Ayudan a programar
> - Evitan que se rompa la app
> - Documentan

# Páginas web

## Los lenguajes de Internet

> - HTML5 ![html5](images/HTML5.png){ .icon }
> - CSS3
> - Javascript
> - Inglés

# Muchas Gracias

## Preguntas
