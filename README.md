# Building

Use pandoc to build the slide

    ./build

Run foreman for continuous build:

    foreman start

# Requirements

- pandoc
- foreman
- browser-sync
